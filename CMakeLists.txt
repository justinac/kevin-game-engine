cmake_minimum_required(VERSION 3.11)
project(kevinEngine
  LANGUAGES C
  VERSION 0.6.3)

list(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/deps/cmake-git/)
include(Dependency)

set(CMAKE_EXPORT_COMPILE_COMMANDS OFF)
set(DEPS_DIR ${PROJECT_SOURCE_DIR}/deps)

find_package(OpenGL REQUIRED)
# find_package(Threads REQUIRED)

git(
  GLFW
  https://github.com/glfw/glfw
  8d3595fb4d4d919e27e1a755095ae1ffae5f50be)

cache(GLFW_BUILD_EXAMPLES OFF BOOL)
cache(GLFW_BUILD_TESTS OFF BOOL)
cache(GLFW_BUILD_DOCS OFF BOOL)

add_library(glad STATIC)
target_sources(glad PRIVATE ${DEPS_DIR}/glad/glad.c)
target_include_directories(glad PUBLIC ${DEPS_DIR}/glad/include)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "../build")

add_executable(kevinEngine)
target_include_directories(kevinEngine PUBLIC ${DEPS_DIR}/stb STATIC)

target_sources(kevinEngine
  PRIVATE
    src/main.c
    src/core/util/input.c
    src/core/util/math.c
    src/core/util/dlists.c
    src/core/gui/window.c
    src/core/gfx/renderer.c
    src/core/gfx/mesh.c
    src/core/gfx/texture.c
    src/core/gfx/shader.c
    src/core/models/entity.c
    src/core/models/scene.c
    src/core/models/camera.c
    src/core/physics/assorted.c)

target_link_libraries(kevinEngine
  PUBLIC
    OpenGL::GL
    OpenGL::GLU
    GLFW::GLFW
    # Threads::Threads
    glad)
