#ifndef INPUT_H
#define INPUT_H

#include <GLFW/glfw3.h>

#include "types.h"

boolean input_get_key(u32 key, u32 state);

#endif // INPUT_H
