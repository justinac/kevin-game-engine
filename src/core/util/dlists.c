#include "dlists.h"

void dlist_init(dList *list, u16 memSize) {
    list->capacity   = 1;
    list->length     = list->capacity;
    list->data       = malloc(memSize);
}

void dlist_resize(dList *dlist, u16 size) {
    if (dlist->length >= dlist->capacity) {
        dlist->capacity++;
    }
}

void dlist_add(dList *dlist, void *item) {
    if (dlist->capacity == dlist->length) {
        dlist_resize(dlist, dlist->capacity++);
    }

    dlist->data[dlist->length++] = item;
}

void dlist_set(dList *dlist, u16 index, void *item) {
    if (index >= 0 && index < dlist->length) {
        dlist->data[index] = item;
    }
}

void *dlist_get(dList *dlist, u16 index) {
    if (index >= 0 && index < dlist->length) {
        return dlist->data[index];
    }

    return NULL;
}

void dlist_remove(dList *dlist, u16 index) {
    if (index < 0 || index >= dlist->length) {
        return;
    }

    dlist->data[index] = NULL;

    int i;
    for (i = index; i < dlist->length - 1; i++) {
        dlist->data[i] = dlist->data[i + 1];
        dlist->data[i + 1] = NULL;
    }

    dlist->data--;

    if (dlist->length > 0 && dlist->length == dlist->capacity / 4) {
        dlist_resize(dlist, dlist->capacity / 2);
    }
}

void dlist_free(dList *dlist) {
    dlist->length = 0;
    dlist->capacity = 0;
    free(dlist->data);
    dlist->data = NULL;
}
