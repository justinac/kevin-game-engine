#include "entity.h"

Entity entity_create(Mesh mesh, vec3 position, vec3 scale) {
    Entity entity;
    entity.mesh                 = mesh;
    entity.position             = position;
    entity.scale                = scale;
    entity.rotation             = 0;
    entity.rotation_velocity    = 0;
    entity.velocity             = (vec3){ 0, 0, 0};

    return entity;
}

mat4 t, s, r, newMatrix;
void entity_update(Entity *entity) {
    mat4_identity(&t);
    mat4_translate(&t, entity->position);
    
    mat4_identity(&r);
    mat4_rotate(&r, entity->rotation);

    mat4_identity(&s);
    mat4_scale(&s, entity->scale);

    mat4_mul(&newMatrix, t, r);
    mat4_mul(&newMatrix, newMatrix, s);

    entity->size.x = entity->mesh.size.x * entity->scale.x;
    entity->size.y = entity->mesh.size.y * entity->scale.y;
    entity->size.z = entity->mesh.size.z * entity->scale.z;

    entity->position.x += entity->velocity.x;
    entity->position.y += entity->velocity.y;
    entity->position.z += entity->velocity.z;

    entity->rotation += entity->rotation_velocity;

    entity->mesh.transformationMatrix = newMatrix;
}