#include "scene.h"

void scene_init(Scene *scene) {
    scene->size = 0;
    scene->capacity = DEFAULT_SCENE_CAPACITY;
    scene->data = malloc(sizeof(Entity) * scene->capacity);
}

void scene_resize(Scene *scene) {
    scene->capacity++;
    scene->data = realloc(scene->data, sizeof(Entity) * scene->capacity);
}

void scene_append(Scene *scene, Entity *item) {
    scene_resize(scene);
    scene->data[scene->size++] = item;
}

Entity scene_get(Scene *scene, u32 index) {
    if (index >= scene->size || index < 0) {
        printf("index %d out of bounds for scene of size %d\n", index, scene->size);
    }

    return *scene->data[index];
}

void scene_set(Scene *scene, u32 index, Entity *item) {
    while (index >= scene->size) {
        scene_append(scene, 0);
    }

    scene->data[index] = item;
}

void scene_free(Scene *scene) {
    free(scene->data);
    scene->size = 0;
}