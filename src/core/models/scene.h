#ifndef SCENE_H
#define SCENE_H

#define DEFAULT_SCENE_CAPACITY 0
#include <stdio.h>
#include <stdlib.h>

#include "../util/types.h"

#include "entity.h"

typedef struct {
    u32     size;
    u32     capacity;
    Entity **data;
} Scene;

void scene_init(Scene *scene);
void scene_resize(Scene *scene);
void scene_append(Scene *scene, Entity *item);
Entity scene_get(Scene *scene, u32 index);
void scene_set(Scene *scene, u32 index, Entity *item);
void scene_free(Scene *scene);

#endif // SCENE_H