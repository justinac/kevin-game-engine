#include "assorted.h"

boolean isColliding(Entity *entity1, Entity *entity2) {
    if (entity1->position.x + entity1->size.x > entity2->position.x - entity2->size.x &&
        entity1->position.x - entity1->size.x < entity2->position.x + entity2->size.x &&
        entity1->position.y + entity1->size.y > entity2->position.y - entity2->size.y &&
        entity1->position.y - entity1->size.y < entity2->position.y + entity2->size.y) {
        return true;
    }
    return false;
} 
