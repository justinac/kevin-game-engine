#include "mesh.h"

Mesh mesh_generate(Texture texture) {
    float hw = texture.width;
    float hh = texture.height;

    float vertices[] = {
        -hw, hh, 0.0f,
         hw, hh, 0.0f,
         hw,-hh, 0.0f,
        -hw,-hh, 0.0f
    };

    float texcoords[] = {
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f
    };

    u16 indices[] = {
        0, 1, 3,
        1, 2, 3
    };

    u32 vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    u32 vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, 0, 0, 0);
    glEnableVertexAttribArray(0);

    u32 vto;
    glGenBuffers(1, &vto);
    glBindBuffer(GL_ARRAY_BUFFER, vto);
    glBufferData(GL_ARRAY_BUFFER, sizeof(texcoords), texcoords, GL_STATIC_DRAW);
    glVertexAttribPointer(1, 2, GL_FLOAT, 0, 0, 0);
    glEnableVertexAttribArray(1);

    u32 ibo;
    glGenBuffers(1, &ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    glBindVertexArray(0);

    mat4 transformationMatrix;
    mat4_identity(&transformationMatrix);

    Mesh mesh;
    mesh.size                   = (vec3) { texture.width, texture.height, 0.0f };
    mesh.vaoID                  = vao;
    mesh.indiceCount            = sizeof(indices) / sizeof(u16);
    mesh.texture                = texture;
    mesh.transformationMatrix   = transformationMatrix;

    return mesh;
}

