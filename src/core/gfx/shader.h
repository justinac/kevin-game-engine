#ifndef SHADER_H
#define SHADER_H


#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include <glad/glad.h>

#include "../util/types.h"

char *file_read(const char* file_path, char* mode);
u32 shader_compile(const char* filePath, int type);
u16 shader_load(const char *vertexFilePath, const char *fragmentFilePath);
void shader_use(u32 programID);

#endif // SHADER_H