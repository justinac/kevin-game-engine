#version 330 core

layout (location = 0) in vec3 position_data;
layout (location = 1) in vec2 texture_coord_data;

out vec2 TexCoord;

uniform mat4 mvp;

void main(void) {
    gl_Position = mvp * vec4(position_data, 1.0);
    TexCoord = texture_coord_data;
}